import time

from process import PROCESS
from operator import itemgetter
from enum import Enum


class PlanetType1(Enum):
    uninitialized = 0
    asteroid = 1
    gas_giant = 2
    normal = 3


class PlanetType2(Enum):
    toxic = 0
    radiated = 1
    barren = 2
    desert = 3
    tundra = 4
    ocean = 5
    swamp = 6
    arid = 7
    terran = 8
    gaia = 9


class PlanetType3(Enum):
    normal = 0
    stable_wormhole = 1
    space_debris = 2
    pirate_cache = 3
    gold_deposits = 4
    gem_deposits = 5
    natives = 6
    splinter_colony = 7
    marooned_hero = 8
    space_monster = 9
    ancient_artifacts = 10
    normal2 = 11
    bombarded = 12
    hostile = 13
    life_supporting = 14
    lifeless = 15


class PlanetSize(Enum):
    tiny = 0
    small = 1
    medium = 2
    large = 3
    huge = 4


class PlanetMinerals(Enum):
    ultra_poor = 0
    poor = 1
    abundant = 2
    rich = 3
    ultra_rich = 4


class PlanetGravity(Enum):
    low = 0
    normal = 1
    high = 2


def byte_literal_to_str(byte_literal):
    return byte_literal.decode('windows-1252').lstrip('\x00').split('\x00', 1)[0]


def base():
    return PROCESS.read_address(0x1D4A380, 'q')


def read_star_list():
    base_address = base() + 0x1092c
    stars = []
    for i in range(100):
        address = base_address + 0x71 * i
        starname = byte_literal_to_str(PROCESS.read_address(address, '15s'))
        if starname == '':
            break
        planet_indices = PROCESS.read_address(address + 0x4a, '5H')
        planet_indices = [-1 if i == 65535 else i for i in planet_indices]
        stars.append((starname, planet_indices))

    return stars


def read_planet_list(planet_count):
    base_address = base() + 0x68E044
    planets = []
    for i in range(planet_count):
        address = base_address + 17 * i
        planet_data = PROCESS.read_address(address, '16B')
        u1, u2, u3, u4, type1, size, grav, u5, type2, u6, mineral, u7, u8, u9, type3, u10 = planet_data
        planets.append((
            PlanetType1(type1),
            PlanetSize(size),
            PlanetGravity(grav),
            PlanetType2(type2),
            PlanetMinerals(mineral),
            PlanetType3(type3)
        ))

    return planets


def read_player_list():
    base_address = base() + 0x696059
    player_races = []
    for i in range(8):
        address = base_address + 0xEA9 * i
        player_race = byte_literal_to_str(PROCESS.read_address(address, '16s'))
        player_races.append(player_race)

    return player_races


def planet_max_pop(planet, num_star_planets, sakkra=True, stellar_converter=False):
    type1, size, grav, type2, mineral, type3 = planet

    toxic_sakkra_values = [10, 14, 17, 20, 23]
    toxic_values = [8, 10, 11, 12, 13]
    sakkra_values = [14, 21, 28, 35, 42]
    values = [12, 17, 22, 27, 32]

    if type1 == PlanetType1.gas_giant:
        size = PlanetSize.huge
        type2 = PlanetType2.barren

    can_convert = stellar_converter and num_star_planets > 1

    if (type1 == PlanetType1.asteroid or size.value < 3) and can_convert:
        size = PlanetSize.large
        type2 = PlanetType2.barren

    if type2 == PlanetType2.toxic and not stellar_converter:
        if sakkra:
            return toxic_sakkra_values[size.value]

        return toxic_values[size.value]

    if size == PlanetSize.huge and type2 == PlanetType2.toxic:
        size = PlanetSize.large

    if sakkra:
        return sakkra_values[size.value]

    return values[size.value]


def star_has_habitable(planets, planet_indices):
    for planet_index in planet_indices:
        type1, size, grav, type2, mineral, type3 = planets[planet_index]
        if type1 == PlanetType1.normal:
            return True

    return False


def max_population(sakkra=True, stellar_converter=False):
    try:
        stars = read_star_list()
        planet_indices = [indices for star_name, indices in stars]
        planet_indices = [index for indices in planet_indices for index in indices]
        if len(planet_indices) < 1:
            return 0
        max_index = max(planet_indices)
        planets = read_planet_list(max_index + 1)
    except Exception:
        return 0

    universe_max_pop = 0

    for star in stars:
        star_name, planet_indices = star
        planet_indices = [i for i in planet_indices if i != -1]
        star_max_pop = 0

        if star_has_habitable(planets, planet_indices):
            for planet_index in planet_indices:
                star_max_pop += planet_max_pop(planets[planet_index], len(planet_indices), sakkra, stellar_converter)

        universe_max_pop += star_max_pop

    return universe_max_pop


def main():
    PROCESS.open_process_from_name('DOSBox.exe')
    races = read_player_list()
    print('max pop', max_population(True, False))
    print('max pop stellar converting', max_population(True, True))
    print('sakkra', 'Sakkra' in races)
    print('psilon', 'Psilon' in races)


if __name__ == '__main__':
    main()
