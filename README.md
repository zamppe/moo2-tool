Python helper script for grinding ideal high score in Master of Orion 2 GOG version.

The script prints:

* Maximum available population for the generated universe, assuming sakkra is an available race 
* Whether sakkra is in the game 
* Whether psilon is in the game

Tested on DOSBox 0.74-2.1, MOO2 1.40b23